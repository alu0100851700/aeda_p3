#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include "numero.hpp"
#include "natural.hpp"
using namespace std;


class entero_t: public numero_t {
public:
  int data_;
  
public:
  entero_t();
  entero_t(const int);
  entero_t(const entero_t&);
  ~entero_t();

  int get() const;
  void set(int);
  
  entero_t& operator = (const int);
  entero_t& operator = (const entero_t&);
  
  entero_t operator + (int);
  entero_t operator + (entero_t&);
  
  entero_t operator - ();
  entero_t operator - (int);
  entero_t operator - (entero_t&);
  
  entero_t operator * (int);
  entero_t operator * (entero_t&);
  
  int operator / (int);
  int operator / (entero_t&);
  int operator / (natural_t&);
  
  int operator % (int);
  int operator % (entero_t&);
  int operator % (natural_t&);
  
  bool operator == (int);
  bool operator == (entero_t&);
  
  bool operator != (int);
  bool operator != (entero_t&);
  
  bool operator > (int);
  bool operator > (entero_t&);
  
  bool operator >= (int);
  bool operator >= (entero_t&);
  
  bool operator < (int);
  bool operator < (entero_t&);
  
  bool operator <= (int);
  bool operator <= (entero_t&);
  
  virtual ostream& toStream(ostream& out) const{
    out << get();
  	return out;
  }
      
  virtual istream& fromStream(istream& in) const{
  	return in;
  }
  
};