#pragma once
#include "base_node.hpp"

template<class T>
class node_t: public base_node_t{
    private:
        T data_;
    public:
        node_t(const T& data):
            data_(data){}
        
        ~node_t(void){}
        
        T get_data(void) const{
            return data_;
        }
        
        void set_data(const T& data){
            data_ = data;
        }

        virtual ostream& toStream(ostream& out) const{
            out << get_data();
            return out;
          }
              
          virtual istream& fromStream(istream& in) const{
            return in;
          }
};