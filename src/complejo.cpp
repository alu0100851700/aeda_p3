#include "complejo.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
complejo_t::complejo_t(void):
	real_(0.0),
	img_(0.0){}

complejo_t::complejo_t(double real, double img):
	real_(real),
	img_(img){}

complejo_t::complejo_t(const real_t& real, const real_t& img):
	real_(real.get()),
	img_(img.get()){}


complejo_t::complejo_t(complejo_t& comp):
	real_(comp.get_real()),
	img_(comp.get_img()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
complejo_t::~complejo_t(void){}


//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
double complejo_t::get_real(void) const{
	return real_.get();
}

void complejo_t::set_real(double real){
	real_ = real;
}

void complejo_t::set_real(const real_t& real){
	real_ = real.get();
}

double complejo_t::get_img(void) const{
	return img_.get();
}

void complejo_t::set_img(double img){
	img_ = img;
}

void complejo_t::set_img(const real_t& img){
	img_ = img.get();
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
complejo_t& complejo_t::operator=(complejo_t& op){
	real_ = op.get_real();
	img_ = op.get_img();
	return *this;
}

complejo_t complejo_t::operator+(complejo_t& op){
	real_t real = real_.get() + op.get_real();
	real_t img = img_.get() + op.get_img();
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator-(complejo_t& op){
	real_t real = real_.get() - op.get_real();
	real_t img = img_.get() - op.get_img();
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator*(complejo_t& op){
	real_t real = (real_.get() * op.get_real()) - (img_.get() * op.get_img());
	real_t img = (real_.get() * op.get_img()) + (img_.get() * op.get_real());
	complejo_t aux(real,img);
	return aux;
}

complejo_t complejo_t::operator/(complejo_t& op){
	real_t real = ((real_.get() * op.get_real()) + (img_.get() * op.get_img())) / ((op.get_real() * op.get_real()) + op.get_img() * op.get_img());
	real_t img = ((img_.get() * op.get_real()) - (real_.get() * op.get_img())) / ((op.get_real() * op.get_real()) + op.get_img() * op.get_img());
	complejo_t aux(real,img);
	return aux;
}

bool complejo_t::operator==(complejo_t& op){
	return (abs(real_.get()-op.get_real()) < EPSILON) && (abs(img_.get() - op.get_real()) < EPSILON);
}

bool complejo_t::operator!=(complejo_t& op){
	return (abs(real_.get()-op.get_real()) > EPSILON) || (abs(img_.get() - op.get_real()) > EPSILON);
}

bool complejo_t::operator>(complejo_t& op){
	double mod1 = sqrt((img_.get() * img_.get()) + (real_.get() * real_.get()));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) > EPSILON);
}

bool complejo_t::operator>=(complejo_t& op){
	double mod1 = sqrt((img_.get() * img_.get()) + (real_.get() * real_.get()));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) > -EPSILON);
}

bool complejo_t::operator<(complejo_t& op){
	double mod1 = sqrt((img_.get() * img_.get()) + (real_.get() * real_.get()));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) < -EPSILON);
}

bool complejo_t::operator<=(complejo_t& op){
	double mod1 = sqrt((img_.get() * img_.get()) + (real_.get() * real_.get()));
	double mod2 = sqrt(op.get_img() * op.get_img() + op.get_real() * op.get_real());
	return ((mod1-mod2) < EPSILON);
}


