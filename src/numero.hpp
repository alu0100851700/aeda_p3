#pragma once
#include <iostream>
#include "exception.hpp"
using namespace std;

class numero_t{
  public:
    virtual ostream& toStream(ostream& os) const = 0;
    virtual istream& fromStream(istream&) const = 0;
    friend ostream& operator<<(ostream&, const numero_t&);
};


