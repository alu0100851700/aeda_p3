#include "racional.hpp"


int mcd(int num1, int num2) {
    int mcd = 0;
    int a = std::max(num1, num2);
    int b = std::min(num1, num2);
    do {
        mcd = b;
        b = a%b;
        a = mcd;
    } while(b!=0);
    return mcd;
}


racional_t::racional_t():
	num_(0),
	denom_(0){}

racional_t::racional_t(const racional_t& rac):
	num_(rac.get_num()),
	denom_(rac.get_denom()){}

racional_t::racional_t(int num, int denom){
	try{
		if(denom == 0) throw denominador_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	if(num%denom != 0){
		int m;
		
		do{
			m = mcd(num,denom);
			num = num/m;
			denom = denom/m;
		}while( m > 1);

		if(num<0 && denom<0){
			num_ = abs(num);
			denom_ = abs(denom);
		}
		else if(num<0 || denom<0){
			num_ = -abs(num);
			denom_ = abs(denom);
		}
		else{
			num_ = num;
			denom_ = denom;
		}
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

racional_t::racional_t(const entero_t& num, const entero_t& denom){
	try{
		if(denom.get() == 0) throw denominador_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	num_ = num.get();
	denom_ = denom.get();

	if(num_%denom_ != 0){
		int m;
		
		do{
			m = mcd(num_.get(),denom_.get());
			num_ = num_.get()/m;
			denom_ = denom_.get()/m;
		}while( m > 1);

		if(num_<0 && denom_<0){
			num_ = abs(num_.get());
			denom_ = abs(denom_.get());
		}
		else if(num_<0 || denom_<0){
			num_ = -abs(num_.get());
			denom_ = abs(denom_.get());
		}
		else{
			num_ = num.get();
			denom_ = denom.get();
		}
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

racional_t::~racional_t(){}

void racional_t::set_num(const int num){
	num_ = num;
	if(num_%denom_ != 0){
		int m;
		do{
			m = mcd(num_.get(),denom_.get());
			num_ = num_.get()/m;
			denom_ = denom_/m;
		}while( m != 1);
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

void racional_t::set_num(const entero_t& num){
	num_ = num.get();
	
	if(num_%denom_ != 0){
		int m;
		do{
			m = mcd(num_.get(),denom_.get());
			num_ = num_.get()/m;
			denom_ = denom_.get()/m;
		}while( m != 1);
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

void racional_t::set_denom(const int denom){
	try{
		if(denom == 0) throw denominador_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	denom_ = denom;
	if(num_%denom_ != 0){
		int m;
		do{
			m = mcd(num_.get(),denom_.get());
			num_ = num_/m;
			denom_ = denom_/m;
		}while( m != 1);

		if(denom < 0){
			num_ = -num_;
			denom_ = abs(denom_.get());
		}
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

void racional_t::set_denom(const entero_t& denom){
	try{
		if(denom.get() == 0) throw denominador_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	entero_t aux = denom;
	if(num_%aux != 0){
		int m;
		do{
			m = mcd(num_.get(),aux.get());
			num_ = num_.get()/m;
			aux = aux.get()/m;
		}while( m != 1);

		if(denom_ < 0){
			num_ = -num_.get();
			aux = abs(aux.get());
		}
		denom_ = aux.get();
	}
	else
		cerr << "Lo introducido no es un numero racional";
}

int racional_t::get_num() const{
	return num_.get();
}

unsigned int racional_t::get_denom() const{
	return denom_.get();
}

real_t racional_t::to_real(){
	return num_/denom_;
}

racional_t& racional_t::operator=(racional_t& data){
	num_ = data.get_num();
	denom_ = data.get_denom();
	return *this;
}

racional_t racional_t::operator+(racional_t& op){
	racional_t aux(num_.get()+op.get_num(), denom_.get()+op.get_denom());
	return aux;
}

racional_t racional_t::operator-(racional_t& op){
	racional_t aux(num_.get()-op.get_num(), denom_.get()-op.get_denom());
	return aux;
}

racional_t racional_t::operator*(racional_t& op){
	racional_t aux(num_.get()*op.get_num(), denom_.get()*op.get_denom());
	return aux;
}

racional_t racional_t::operator/(racional_t& op){
	try{
		if(op.get_num() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	racional_t aux(num_.get()*op.get_denom(), denom_.get()*op.get_num());
	return aux;
}

bool racional_t::operator==(racional_t& op){
	return (num_ == op.get_num() && denom_ == op.get_denom());
}

bool racional_t::operator!=(racional_t& op){
	return (num_ != op.get_num() || denom_ != op.get_denom());
}

bool racional_t::operator>(racional_t& op){
	return (to_real().get() - op.to_real().get() > EPSILON);
}

bool racional_t::operator>=(racional_t& op){
	return (to_real().get() - op.to_real().get() > -EPSILON);
}

bool racional_t::operator<(racional_t& op){
	return (to_real().get() - op.to_real().get() < -EPSILON);
}

bool racional_t::operator<=(racional_t& op){
	return ((to_real().get()-op.to_real().get()) < EPSILON);
}

