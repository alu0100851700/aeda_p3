#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include "numero.hpp"
using namespace std;

class real_t: public numero_t {
public:
  double data_;
  
public:
  real_t();
  real_t(double);
  real_t(const real_t& ent);
  ~real_t();

  double get() const;
  void set(double);
  
  real_t& operator = (double);
  real_t& operator = (real_t&);
  
  real_t operator + (double);
  real_t operator + (real_t&);
  
  real_t operator - (double);
  real_t operator - (real_t&);
  
  real_t operator * (double);
  real_t operator * (real_t&);
  
  real_t operator / (double);
  real_t operator / (real_t&);
  
  bool operator == (double);
  bool operator == (real_t&);
  
  bool operator != (double);
  bool operator != (real_t&);
  
  bool operator > (double);
  bool operator > (real_t&);
  
  bool operator >= (double);
  bool operator >= (real_t&);
  
  bool operator < (double);
  bool operator < (real_t&);
  
  bool operator <= (double);
  bool operator <= (real_t&);
  
  virtual ostream& toStream(ostream& out) const{
    out << get();
		return out;
  }
      
  virtual istream& fromStream(istream& in) const{
  	return in;
  }
};