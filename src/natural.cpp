#include "natural.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
natural_t::natural_t(): 
	data_(0){}

natural_t::natural_t(unsigned int data) 
	{
		try{
			if(data >= 0) data_ = data;
			else	throw valor_fuera_de_rango();
		}
		catch(exception& ex){
			cerr << ex.what() << endl;
		}
	}

natural_t::natural_t(const natural_t& op):
	data_(op.get()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
natural_t::~natural_t(){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
unsigned int natural_t::get() const{
 return data_; 
}

void natural_t::set(long int data){
	try{
		if(data >= 0) data_ = data;
		else	throw valor_fuera_de_rango();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
natural_t& natural_t::operator=(long int data){
	try{
		if(data >= 0) data_ = data;
		else	throw valor_fuera_de_rango();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return *this;
}
natural_t& natural_t::operator=(const natural_t& nat){
	data_ = nat.get();
	return *this;
}

/////////////////////////////////////////
natural_t natural_t::operator+(unsigned int op){
	natural_t aux(data_ + op);
	return aux;
}
natural_t natural_t::operator+(natural_t& op){
	natural_t aux(data_ + op.get());
	return aux;
}

/////////////////////////////////////////
natural_t natural_t::operator-(unsigned int op){
	try{
		if(data_ - op < 0) throw valor_fuera_de_rango();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	natural_t aux(data_ - op);
	return aux;
}
natural_t natural_t::operator-(natural_t& op){
	try{
		if(data_ - op.get() < 0) throw valor_fuera_de_rango();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	natural_t aux(data_ - op.get());
	return aux;
}

/////////////////////////////////////////
natural_t natural_t::operator*(unsigned int op){
	natural_t aux(data_ * op);
	return aux;
}
natural_t natural_t::operator*(natural_t& op){
	natural_t aux(data_ * op.get());
	return aux;
}
int natural_t::operator*(int op){
 return  ((int)data_ * op);
}

/////////////////////////////////////////
double natural_t::operator/(int op){
	try{
		if(op == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
 	return  ((int)data_ / op);
}

double natural_t::operator/(natural_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
 return  (data_ / op.get());
}

/////////////////////////////////////////
bool natural_t::operator==(int op){
 return ((int)data_ == op); 
}
bool natural_t::operator==(natural_t& op){
 return (data_ == op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator!=(int op){
 return ((int)data_ != op); 
}
bool natural_t::operator!=(natural_t& op){
 return (data_ != op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator>(int op){
 return ((int)data_ > op); 
}
bool natural_t::operator>(natural_t& op){
 return (data_ > op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator>=(int op){
 return ((int)data_ >= op); 
}
bool natural_t::operator>=(natural_t& op){
 return (data_ >= op.get()); 
}

/////////////////////////////////////////
bool natural_t::operator<(int op){
  return ((int)data_ < op);
}
bool natural_t::operator<(natural_t& op){
  return (data_ < op.get());
}

/////////////////////////////////////////
bool natural_t::operator<=(int op){
  return ((int)data_ <= op);
}
bool natural_t::operator<=(natural_t& op){
  return (data_ <= op.get());
}

/////////////////////////////////////////
