#pragma once
#include <iostream>
#include <exception>
using namespace std;

class valor_fuera_de_rango: public exception
{
	virtual const char* what() const throw(){
		return "Valor fuera de rango";
	}
};

class division_por_cero: public exception
{
	virtual const char* what() const throw(){
		return "Division entre cero";
	}
};

class denominador_cero: public exception
{
	virtual const char* what() const throw(){
		return "Denominador no valido";
	}
};

class posicion_incorrecta: public exception
{
	virtual const char* what() const throw(){
		return "Acceso a posicion incorrecta";
	}
};

class lista_vacia: public exception
{
	virtual const char* what() const throw(){
		return "La lista se encuentra vacia";
	}
};