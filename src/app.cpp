#include <cstring>
#include "natural.hpp"
#include "entero.hpp"
#include "real.hpp"
#include "racional.hpp"
#include "complejo.hpp"
#include "numero.hpp"
#include "list.hpp"
#include "stack.hpp"
#include "queue.hpp"
#include "vector.hpp"
#define TRAZA(t) cout << (#t) << " = " << (t) << endl

using namespace std;

int main(){

	natural_t nat(10);
	entero_t ent(-4);
	real_t real(-4.6);
	racional_t racional(-4, 3);
	complejo_t complejo(-4.7, 3.5);

	nat = -1; //Excepción

	list_t<numero_t*> lista;
	stack_t<numero_t*> pila;
	queue_t<numero_t*> cola;
	vector_t<numero_t*> vector(4);

	cout << "\n----- LISTA -----" << endl;
	lista.push_front(&nat);
	lista.push_front(&ent);
	lista.push_front(&real);
	lista.push_front(&racional);
	lista.push_front(&complejo);
	
	lista.pop_front() -> toStream(cout);
	cout << " -> ";
	lista.pop_front() -> toStream(cout);
	cout << " -> ";
	lista.pop_front() -> toStream(cout);
	cout << " -> ";
	lista.pop_front() -> toStream(cout);
	cout << " -> ";
	lista.pop_front() -> toStream(cout);
	cout << endl;
	lista.pop_front();	//Excepcion
	cout << endl;

	cout << "\n----- PILA -----" << endl;
	pila.push(&nat);
	pila.push(&ent);
	pila.push(&real);
	pila.push(&racional);
	pila.push(&complejo);

	pila.pop() -> toStream(cout);
	cout << endl;
	pila.pop() -> toStream(cout);
	cout << endl;
	pila.pop() -> toStream(cout);
	cout << endl;
	pila.pop() -> toStream(cout);
	cout << endl;
	pila.pop() -> toStream(cout);
	cout << endl;
	pila.pop();	//Excepcion
	cout << endl;


	cout << "\n----- COLA -----" << endl;
	cola.push(&nat);
	cola.push(&ent);
	cola.push(&real);
	cola.push(&racional);
	cola.push(&complejo);

	cola.pop() -> toStream(cout);
	cout << endl;
	cola.pop() -> toStream(cout);
	cout << endl;
	cola.pop() -> toStream(cout);
	cout << endl;
	cola.pop() -> toStream(cout);
	cout << endl;
	cola.pop() -> toStream(cout);
	cout << endl;
	cola.pop();
	cout << endl;


	vector[0] = &nat;
	vector[1] = &ent;
	vector[2] = &real;
	vector[3] = &racional;
	vector[4] = &complejo; //Excepcion



	cout << endl;

	return 0;
}