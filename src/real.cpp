#include "real.hpp"

//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
real_t::real_t():
	data_(0.0){}

real_t::real_t(double data):
	data_(data){}

real_t::real_t(const real_t& ent):
	data_(ent.get()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
real_t::~real_t(){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
double real_t::get() const{
 return data_; 
}

void real_t::set(double data){
 data_ = data; 
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
real_t& real_t::operator=(double data){
 data_ = data;
 return *this;
}
real_t& real_t::operator=(real_t& data){
 data_ = data.get();
 return *this;
}

//////////////////////////////////////////
real_t real_t::operator+(double op){
	real_t aux(data_ + op);
	return  aux;
}
real_t real_t::operator+(real_t& op){
	real_t aux(data_ + op.get());
 	return  aux;
}

//////////////////////////////////////////
real_t real_t::operator-(double op){
	real_t aux(data_ - op);
	return aux;
}
real_t real_t::operator-(real_t& op){
	real_t aux(data_ - op.get());
	return aux;
}

//////////////////////////////////////////
real_t real_t::operator*(double op){
	real_t aux(data_ * op);
	return aux;
}
real_t real_t::operator*(real_t& op){
	real_t aux(data_ * op.get());
	return aux;
}

//////////////////////////////////////////
real_t real_t::operator/(double op){
	try{
		if(op == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}

	return  (data_ / op);
}
real_t real_t::operator/(real_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ / op.get());
}

//////////////////////////////////////////
bool real_t::operator==(double op){
 return (data_ == op); 
}
bool real_t::operator==(real_t& op){
 return (data_ == op.get()); 
}

//////////////////////////////////////////
bool real_t::operator!=(double op){
 return (data_ != op); 
}
bool real_t::operator!=(real_t& op){
 return (data_ != op.get()); 
}

//////////////////////////////////////////
bool real_t::operator>(double op){
 return (data_ > op); 
}
bool real_t::operator>(real_t& op){
 return (data_ > op.get()); 
}

//////////////////////////////////////////
bool real_t::operator>=(double op){
 return (data_ >= op); 
}
bool real_t::operator>=(real_t& op){
 return (data_ >= op.get()); 
}

//////////////////////////////////////////
bool real_t::operator<(double op){
  return (data_ < op);
}
bool real_t::operator<(real_t& op){
  return (data_ < op.get());
}

//////////////////////////////////////////
bool real_t::operator<=(double op){
  return (data_ <= op);
}
bool real_t::operator<=(real_t& op){
  return (data_ <= op.get());
}

