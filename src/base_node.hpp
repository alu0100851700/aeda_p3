#pragma once
#include <cstdio>
#include <iostream>
using namespace std;

class base_node_t{
private:
  base_node_t* next_;
  
public:
  base_node_t(void):
  	next_(NULL){}
  	
  virtual ~base_node_t(void){};

  base_node_t* get_next(void) const{
    return next_;
  }
  void set_next(base_node_t* next){
    next_ = next;
  }

  virtual ostream& toStream(ostream&) const = 0;
  virtual istream& fromStream(istream&) const = 0;

  friend ostream& operator<<(ostream& os, const base_node_t&);
};

ostream& operator<<(ostream& os, const base_node_t& n){
	os << n.toStream(os);
  return os;
}