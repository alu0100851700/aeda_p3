#include "entero.hpp"


//////////////////////////////////////////
// CONSTRUCTORES
/////////////////////////////////////////
entero_t::entero_t():
	data_(0){}

entero_t::entero_t(const int data):
	data_(data){}

entero_t::entero_t(const entero_t& ent):
	data_(ent.get()){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
entero_t::~entero_t(){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
int entero_t::get() const{
 return data_; 
}

void entero_t::set(int data){
 data_ = data; 
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
entero_t& entero_t::operator=(const int data){
 data_ = data;
 return *this;
}
entero_t& entero_t::operator=(const entero_t& data){
 data_ = data.get();
 return *this;
}

//////////////////////////////////////////
entero_t entero_t::operator+(int op){
	entero_t aux(data_ + op);
	return  aux;
}
entero_t entero_t::operator+(entero_t& op){
	entero_t aux(data_ + op.get());
 	return  aux;
}

//////////////////////////////////////////
entero_t entero_t::operator-(){
	entero_t aux(-data_);
	return aux;
}
entero_t entero_t::operator-(int op){
	entero_t aux(data_ - op);
	return aux;
}
entero_t entero_t::operator-(entero_t& op){
	entero_t aux(data_ - op.get());
	return aux;
}

//////////////////////////////////////////
entero_t entero_t::operator*(int op){
	entero_t aux(data_ * op);
	return aux;
}
entero_t entero_t::operator*(entero_t& op){
	entero_t aux(data_ * op.get());
	return aux;
}

//////////////////////////////////////////
int entero_t::operator/(int op){
	try{
		if(op == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ / op);
}
int entero_t::operator/(entero_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ / op.get());
}
int entero_t::operator/(natural_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ / op.get());
}

//////////////////////////////////////////
int entero_t::operator%(int op){
	try{
		if(op == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ % op);
}
int entero_t::operator%(entero_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ % op.get());
}
int entero_t::operator%(natural_t& op){
	try{
		if(op.get() == 0) throw division_por_cero();
	}
	catch(exception& ex){
		cerr << ex.what() << endl;
	}
	return  (data_ % op.get());
}

//////////////////////////////////////////
bool entero_t::operator==(int op){
 return (data_ == op); 
}
bool entero_t::operator==(entero_t& op){
 return (data_ == op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator!=(int op){
 return (data_ != op); 
}
bool entero_t::operator!=(entero_t& op){
 return (data_ != op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator>(int op){
 return (data_ > op); 
}
bool entero_t::operator>(entero_t& op){
 return (data_ > op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator>=(int op){
 return (data_ >= op); 
}
bool entero_t::operator>=(entero_t& op){
 return (data_ >= op.get()); 
}

//////////////////////////////////////////
bool entero_t::operator<(int op){
  return (data_ < op);
}
bool entero_t::operator<(entero_t& op){
  return (data_ < op.get());
}

//////////////////////////////////////////
bool entero_t::operator<=(int op){
  return (data_ <= op);
}
bool entero_t::operator<=(entero_t& op){
  return (data_ <= op.get());
}
