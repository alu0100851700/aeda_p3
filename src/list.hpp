#pragma once
#include "base_list.hpp"
#include "node.hpp"

template <class T>
class list_t{
private:
    base_list_t lista_;
public:
    //CONSTRUCTOR Y DESTRUCTOR
    list_t(void):
        lista_(){}
        
    virtual ~list_t(){}

    //MÉTODOS DE MODIFICACIÓN
    void push_front(const T& dato){
        node_t<T>* aux = new node_t<T>(dato);
        lista_.insert_front(aux);
        
    }
    
    void push_back(const T& dato){

        node_t<T>* aux = new node_t<T>(dato);
        lista_.insert_back(aux);
    }
    
    T pop_front(void){
        try{
            if(empty()) throw   lista_vacia();
            else{
                node_t<T>* aux = lista_.extract_front();
                T dato(aux -> get_data());
                delete aux;
                return dato;
            }
        }
        catch(exception& ex){
            cerr << ex.what() << endl;
        }
        
    }
	
    bool empty(void) const{
        return lista_.empty();
    }

};