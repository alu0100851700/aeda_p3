#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include "numero.hpp"
#include "entero.hpp"
#include "natural.hpp"
#include "real.hpp"
using namespace std;
#define EPSILON 0.001

class racional_t: public numero_t { // ------- num/denom!=0 ------------
private:
  entero_t num_;
  natural_t denom_;
  
public:
  racional_t();
  racional_t(const racional_t&);
  racional_t(int,int);
  racional_t(const entero_t&, const entero_t&);
  
  ~racional_t();
  
  void set_num(const int);
  void set_num(const entero_t&);
  void set_denom(const int);
  void set_denom(const entero_t&);
  
  int get_num() const;
  unsigned int get_denom() const;

  real_t to_real();

  racional_t& operator=(racional_t&);
  
  racional_t operator+(racional_t&);
  
  racional_t operator-(racional_t&);
  
  racional_t operator*(racional_t&);
  
  racional_t operator/(racional_t&);
  
  bool operator==(racional_t&);
  
  bool operator!=(racional_t&);
  
  bool operator>(racional_t&);
  
  bool operator>=(racional_t&);
  
  bool operator<(racional_t&);
  
  bool operator<=(racional_t&);
  
  virtual ostream& toStream(ostream& os) const{
    os << get_num() << "/" << get_denom();
		return os;
  }
      
  virtual istream& fromStream(istream& is) const{
  	return is;
  }
  
  friend int mcd(int,int);
};