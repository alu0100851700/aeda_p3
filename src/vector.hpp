#pragma once
#include <cstdio>
#include <iostream>
using namespace std;

template <class T>
class vector_t{
private:
    T* v_;
    int first_;
    int last_;

public:
    // CONSTRUCTOR, CONSTRUCTOR DE COPIA Y DESTRUCTOR
    vector_t(const int lenght):
        first_(0),
        last_(lenght-1)
    {
        try{
            v_ = new T[lenght];
        }
        catch(...){
            cerr << "Fallo en la reserva de memoria" << endl;
        }
        
    }
        
    vector_t(vector_t& v){
        try{
            v_ = new T[v.size()];
        }
        catch(...){
            cerr << "Fallo en la reserva de memoria" << endl;
        }

        first_ = v.get_first();    last_ = v.get_last();
        for(int i=first_; i<=last_; i++)
    	v_[i] = v[i];
    }
    
    ~vector_t(){
    	delete v_;
    }

    // ACCESO Y MODIFICACION
    int get_first() const{
        return first_;
    }
    int get_last() const{
        return last_;
    }
    void set_first(const int& first){
        int aux = last_ - first_;
        first_ = first;
        last_ = first_ + aux;
    }
    
    int size(){
        return (last_ - first_ + 1); 
    }
    
    //SOBRECARGA DE OPERADORES
    T& operator[](const int& pos){
        try{
            if (pos < first_ || pos > last_)    throw posicion_incorrecta();
        }
        catch(exception& ex){
            cerr << ex.what() << endl;
        }
        return v_[pos-first_];
        
    }
    
    vector_t& operator=(const vector_t& v){
        if(size() != v.size()){
    	delete v_;

        try{
            v_ = new T[v.size()];
        }
        catch(...){
            cerr << "Fallo en la reserva de memoria" << endl;
        }
    	
        }
        first_ = v.get_first();    last_ = v.get_last();
        for(int i=first_; i<=last_; i++)
    	v_[i] = v[i];
        
        return *this;
    }
    
};