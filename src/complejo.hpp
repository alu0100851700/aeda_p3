#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include "numero.hpp"
#include "real.hpp"
using namespace std;
#define EPSILON 0.001

class complejo_t: public numero_t {
private:
  real_t real_;
  real_t img_;
  
public:
  complejo_t(void);
  complejo_t(double, double);
  complejo_t(const real_t&, const real_t&);
  complejo_t(complejo_t&);
  ~complejo_t(void);

  double get_real(void) const;
  void set_real(double);
  void set_real(const real_t&);
  
  double get_img(void) const;
  void set_img(double);
  void set_img(const real_t&);

  complejo_t& operator=(complejo_t&);
  
  complejo_t operator+(complejo_t&);
  
  complejo_t operator-(complejo_t&);
  
  complejo_t operator*(complejo_t&);
  
  complejo_t operator/(complejo_t&);
  
  bool operator==(complejo_t&);
  
  bool operator!=(complejo_t&);
  
  bool operator>(complejo_t&);
  
  bool operator>=(complejo_t&);
  
  bool operator<(complejo_t&);
  
  bool operator<=(complejo_t&);

  virtual ostream& toStream(ostream& os) const {
    if(get_img()>0)
      os << get_real() << " + " << get_img() << "i";
    else
      os << get_real() << get_img() << "i";
    return os;
  }

  virtual istream& fromStream(istream& is) const {
    return is;
  }
  
};