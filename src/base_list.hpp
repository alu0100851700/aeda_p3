#pragma once

#include "base_node.hpp"
#include "exception.hpp"
#include <cstdio>
using namespace std;

class base_list_t{
private:
  base_node_t* first_;
  base_node_t* last_;

public:
  // CONSTRUCTOR Y DESTRUCTOR
  base_list_t():
  	first_(NULL),
  	last_(NULL){}
  	
  virtual ~base_list_t(){}

  // ACCESO Y MODIFICACION
  base_node_t* extract_front(){
    base_node_t* aux = first_;

    if(first_ == last_) first_ = last_ = NULL;
    else first_ = first_ -> get_next();

    aux -> set_next(NULL);
    return aux;
  }
  
  void insert_front(base_node_t* n){
    if(empty()){
      first_ = n;
      last_ = n;
    }
    else{
      n -> set_next(first_);
      first_ = n;
    }
  }
  
  void insert_back(base_node_t* n){
    if(empty()){
      first_ = n;
      last_ = n;
    }
    else{
      last_ -> set_next(n);
      last_ = n;
    }
  }

  bool empty() const{
    return (first_ == NULL && last_==NULL);
  }
  
  ostream& toStream(ostream& os){
    return os;
  }

  // SOBRECARGA DE OPERADORES
  friend ostream& operator<<(ostream& os, base_list_t&);
};
/*
ostream& operator<<(ostream& os, base_list& l){
	if(!l.empty()){
		base_node* aux = l.get_first();
	    while(aux -> get_next() != NULL){
	        os << *aux << "-> ";
	        aux = aux -> get_next();
	    }

	    os << *aux;
	}
    return os;
}
*/
ostream& operator<<(ostream& os, base_list_t& l){
	os << l.toStream(os);
  return os;
}
