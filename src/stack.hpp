#pragma once
#include "base_list.hpp"

template <class T>
class stack_t{
private:
    base_list_t lista_;
public:
    //CONSTRUCTOR Y DESTRUCTOR
    stack_t(void):
        lista_(){}
        
    virtual ~stack_t(){}

    //MÉTODOS DE MODIFICACIÓN
    void push(const T& dato){
        node_t<T>* aux = new node_t<T>(dato);
        lista_.insert_front(aux);
    }
    
    T pop(void){
        try{
            if(empty()) throw   lista_vacia();
            else{
                node_t<T>* aux = lista_.extract_front();
                T dato(aux -> get_data());
                delete aux;
                return dato;
            }
        }
        catch(exception& ex){
            cerr << ex.what() << endl;
        }
    }
	
    bool empty(void){
        return lista_.empty();
    }
};
