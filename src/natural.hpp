#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include "numero.hpp"
using namespace std;

class natural_t: public numero_t {
public:
  unsigned int data_;
  
public:
  natural_t();
  natural_t(unsigned int);
  natural_t(const natural_t&);
  ~natural_t();

  unsigned int get() const;
  void set(long int);
  
  natural_t& operator = (long int);
  natural_t& operator = (const natural_t&);
  
  natural_t operator+(unsigned int);
  natural_t operator + (natural_t&);
  
  natural_t operator - (unsigned int);
  natural_t operator - (natural_t&);
  
  natural_t operator * (unsigned int);
  natural_t operator * (natural_t&);
  int operator * (int);
  
  double operator / (int);
  double operator / (natural_t&);
  
  bool operator == (int);
  bool operator == (natural_t&);
  
  bool operator != (int);
  bool operator != (natural_t&);
  
  bool operator > (int);
  bool operator > (natural_t&);
  
  bool operator >= (int);
  bool operator >= (natural_t&);
  
  bool operator < (int);
  bool operator < (natural_t&);
  
  bool operator <= (int);
  bool operator <= (natural_t&);
  
  virtual ostream& toStream(ostream& out) const{
  		out << get();
  		return out;
  }
      
  virtual istream& fromStream(istream& in) const{
    return in;
  }
};